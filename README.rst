Simple command line 'todo'
==========================

Simple 'todo' command. It allows to mantain a todo-list in the form of a stack.
Usage:

$ ctodo [option][argument]

Possible options are:
  -h : prints this message
  -a [text]: add a new todo to the stack
  -p : pop the last position of the stack
  -l : list all todos
  -r [number]: removes position indicated by number

----


